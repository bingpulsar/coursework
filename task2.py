import random
import time
def GeneratePlayCard(N):
    '''
    parameter: N 为抽出牌的数量

    扑克牌共52张
    黑桃=Spade || 红桃=Heart || 梅花=Club || 方块=Diamond
     A 对应 14 || J,Q,K 对应 11,12,13
    '''
    all=[]
    for suit in ['S','H','C','D']:
        for number in range(2,15):
            all.append((suit,number))
    return tuple(random.sample(all,N))
def IsStraightFlush(MyCard):
    '''
    parameter: MyCard 为自己手中用于分析的牌
    这个子函数验证是否有同色且连续的五张同花顺牌出现
    '''
    MyCardClassify=[[],[],[],[]]
    StraightFlush=False
    #下面这个循环是对手中的牌按花色分类
    for C,N in MyCard:
        if C == 'S':
            MyCardClassify[0].append(N)
        elif C == 'H':
            MyCardClassify[1].append(N)
        elif C == 'C':
            MyCardClassify[2].append(N)
        elif C == 'D':
            MyCardClassify[3].append(N)
    #下面这个循环是对分类后的花色牌判断有没有连续的5张牌
    for suit in MyCardClassify:
        if len(suit)>=5:
            suit.sort()
            for n in range(len(suit)-4):
                if suit[n+4]-suit[n]==4:
                    StraightFlush=True
    return StraightFlush
def Circulation(N):
    '''
    parameter: N 为循环的次数
    '''
    time_start = time.time()
    T = 0
    for n in range(N):
        IS=IsStraightFlush(GeneratePlayCard(13))
        if IS == True:
            T += 1
    time_end = time.time()
    return T/N,time_end - time_start
if __name__ == '__main__':
    N = 10000
    Prob,time=Circulation(N)
    print('经过{:,}次的样本统计, 耗时{:.4f}s, 出现同花顺的概率为{:.3%}'.format(N,time,Prob))
