import numpy as np
import matplotlib.pyplot as plt
if __name__ == '__main__':
    Fs=256
    x=np.arange(0,1,1/Fs)
    N = len(x)
    F1=np.sin(2*np.pi*100*x)
    F2=np.sin(2*np.pi*x*50)
    F3=2*np.sin(2*np.pi*25*x)
    F4=np.sin(2*np.pi*75*x+(np.pi/2))
    freq = np.arange(N) / N * Fs
    Y = np.fft.fft(F1+F2+F3+F4)/(N/2)
    Y[0] = Y[0] / 2
    freq_half = freq[range(int(N/2))]
    Y_half = Y[range(int(N/2))]
    fig, ax = plt.subplots(6, 1, figsize=(10, 10))
    ax[0].plot(x,F1,label=r"$\sin(200{\pi}x)$")
    ax[0].legend()
    ax[1].plot(x,F2,label=r"$\sin(100{\pi}x)$")
    ax[1].legend()
    ax[2].plot(x,F3,label=r"$2\sin(200{\pi}x)$")
    ax[2].legend()
    ax[3].plot(x,F4,label=r"$\sin(200{\pi}x)+\frac{\pi}{2}$")
    ax[3].legend()
    ax[4].plot(x,F1+F2+F3+F4,label='total')
    ax[4].legend()

    ax[5].plot(freq_half,abs(Y_half))
    
    plt.show()
