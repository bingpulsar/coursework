from itertools import islice 
import re

def Split(Text):
    Chinese_Total='';English_Total=''
    for T in islice(Text,1,None):
        print(T)
        Match = re.search(r'[a-zA-Z]',T)
        if Match:
            Separator = Match.span()[0]
            Chinese = T[:Separator]
            English = T[Separator:]
            Chinese_Total += Chinese + '\n'
            English_Total += English
        else:
            Chinese_Total += T
            English_Total += T
    return Chinese_Total,English_Total
if __name__ == '__main__':
    Text = open('再别康桥.txt','r',encoding='utf-8')
    Chinese,English = Split(Text)
    Text.close()
    with open('再别康桥中文版.txt','w',encoding='utf-8') as o :
        o.write(Chinese)
    with open('再别康桥英文版.txt','w',encoding='utf-8') as o :
        o.write(English)