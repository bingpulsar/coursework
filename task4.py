import numpy as np
import matplotlib.pyplot as plt
if __name__ == '__main__':
    mean = np.array([50,50])
    cov = np.array([[100, 0], [0, 100]])
    #生成高斯分布
    data = np.random.multivariate_normal(mean, cov, size=700000)
    x,y=data.T
    plt.hist2d(x,y, bins=100,vmax=1000,vmin=0, cmap='gray')
    #plt.hexbin(x, y, gridsize=100,vmax=1000,vmin=0,cmap='gray')
    plt.axis('off')
    plt.colorbar()
    plt.show()
