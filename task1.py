# 作业一

# 请输出一个自动出题软件，实现小学数学的加法练习。
# 要求：1. 出10个题，自动判断正确，对的加10分，错的不加分；
#      2. 每道题实现个位数加法，但没有0.
#      3. 10题做完以后显示总分

# 例如：
# 1+2=？3
# 正确
# 2+3=？4
# 错误
# 。。。。

# 您得到80分

import random as rm
def TiMu():
    x0 = 0
    x1 = rm.randint(1,9)
    x2 = rm.randint(1,9)
    x0=x1+x2
    print('\n{}+{}=? '.format(x1,x2,x0))
    return(x0)
if __name__ == '__main__':
    T = 0 
    for n in range(10):
        x0=TiMu()
        try:x=int(input('请输入第 {} 题目的答案:\n'.format(n+1)))
        except:
            print("\n\n[程序错误]请只输入阿拉伯数字! 请重新运行程序!\n\n")
            exit()
        if x == x0:
            T=T+10
            print("\n答对了，加10分，目前得分:{}\n".format(T))
        else:
            print(x)
            print("\n答错了，目前得分:{}\n".format(T))
        if n == 9 :
            print("\n答题结束，总得分:{}\n".format(T))
